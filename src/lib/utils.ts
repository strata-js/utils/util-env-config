// ---------------------------------------------------------------------------------------------------------------------
// Misc. Utilities
// ---------------------------------------------------------------------------------------------------------------------

/**
 * Simple object check.
 *
 * @param item - The item to check.
 *
 * @returns Returns true if the item is an object, false other wise.
 */
export function isObject(item : unknown) : boolean
{
    return (!!item && typeof item === 'object' && !Array.isArray(item) && !(item instanceof Date));
} // end isObject

/**
 * Deep merge two objects. _Note: This replaces arrays, it does not merge them, unlike lodash's `merge` function._
 *
 * @param target - The object to merge into.
 * @param sources - The object(s) to merge into target.
 */
export function mergeDeep(target : Record<string, any>, ...sources : Record<string, any>[]) : Record<string, any>
{
    if(!sources.length)
    {
        return target;
    } // end if

    const source = sources.shift();
    if(isObject(target) && isObject(source))
    {
        for(const key in source)
        {
            // Why is this call weird? Well, see this: https://eslint.org/docs/rules/no-prototype-builtins
            if(Object.prototype.hasOwnProperty.call(source, key))
            {
                if(isObject(source[key]))
                {
                    if(!isObject(target[key]))
                    {
                        target[key] = source[key];
                    } // end if

                    mergeDeep(target[key], source[key]);
                }
                else
                {
                    target[key] = source[key];
                } // end if
            } // end if
        } // end for
    } // end if

    return mergeDeep(target, ...sources);
} // end mergeDeep

// ---------------------------------------------------------------------------------------------------------------------
