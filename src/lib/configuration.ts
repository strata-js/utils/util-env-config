//----------------------------------------------------------------------------------------------------------------------
// ConfigUtil
//----------------------------------------------------------------------------------------------------------------------

import { get, set, camelCase } from 'lodash';

// Utils
import { mergeDeep } from './utils';

//----------------------------------------------------------------------------------------------------------------------

class ConfigUtil
{
    #config : Record<string, any> = {};
    #env = 'local';

    //------------------------------------------------------------------------------------------------------------------

    constructor()
    {
        this.#env = process.env.ENVIRONMENT ?? process.env.NODE_ENVIRONMENT ?? 'local';
    } // end constructor

    //------------------------------------------------------------------------------------------------------------------
    // Properties
    //------------------------------------------------------------------------------------------------------------------

    get config() : Record<string, any> { return this.#config; }
    get env() : string { return this.#env; }

    //------------------------------------------------------------------------------------------------------------------

    /**
     * Gets a configuration path, returning either the value, or a provided default.
     *
     * _Note: This function is primarily to provide a type-safe way of retrieving configuration keys._
     *
     * @param key - A path to the key. (Full lodash `get` syntax supported.)
     * @param defaultVal - The default value to return if nothing is found
     */
    get<T = any>(key : string, defaultVal : T | undefined = undefined) : T
    {
        return get(this.#config, key, defaultVal) as T;
    } // end get

    /**
     * Sets a configuration path to `val`.
     *
     * @param key - A path to the key. (Full lodash `get` syntax supported.)
     * @param val - The value to set.
     */
    set(key : string, val : unknown) : void
    {
        set(this.#config, key, val);
    } // end set

    /**
     * Sets the environment manually.
     *
     * @param env - The environment to set.
     */
    setEnv(env : string) : void
    {
        this.#env = env;
    } // end setEnv

    /**
     * Directly load configuration. This allows you to set the configuration directly, if you're loading it from an
     * external source.
     *
     * @param config - An object with keys and values.
     */
    setConfig(config : Record<string, unknown>) : void
    {
        this.#config = config;
    } // end setConfig

    /**
     * Parse the config, substituting environment variables, and cascading based on environment.
     *
     * @param config - An object with keys and values.
     */
    parseConfig(config : Record<string, unknown>) : void
    {
        // First, we need to build the base config object.
        const { environments, ...baseConfig } = <{ environments : Record<string, unknown> }>config;

        // Environment Config Overrides
        const overrides = <Record<string, unknown>>environments?.[this.#env] ?? {};

        // Find env overrides that start with 'CONFIG.', i.e. 'CONFIG.HTTP.PORT'
        const envOverrides = {};
        for(const [ key, value ] of Object.entries(process.env))
        {
            if(key.startsWith('CONFIG.'))
            {
                const configKey = (key.split('CONFIG.')[1])
                    .split('.')
                    .map(camelCase)
                    .join('.');

                set(envOverrides, configKey, value);
            } // end if
        } // end for

        // Now we apply the environment overrides
        this.setConfig(mergeDeep({}, baseConfig, overrides, envOverrides));
    } // end parseConfig
} // end ConfigUtil

//----------------------------------------------------------------------------------------------------------------------

export default new ConfigUtil();

//----------------------------------------------------------------------------------------------------------------------
