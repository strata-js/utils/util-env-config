// ---------------------------------------------------------------------------------------------------------------------
// Unit Tests for the utils.spec.ts module.
// ---------------------------------------------------------------------------------------------------------------------

import { expect } from 'chai';

// To be tested
import { isObject, mergeDeep } from '../src/lib/utils';

// ---------------------------------------------------------------------------------------------------------------------

describe('Utility Functions', () =>
{
    describe('isObject()', () =>
    {
        it('returns true when the item is an object', () =>
        {
            class Foo {}

            const literal = {};
            const classInst = new Foo();

            // eslint-disable-next-line no-new-object
            const objInst = new Object();

            let result = isObject(literal);
            expect(result).to.be.true;

            result = isObject(classInst);
            expect(result).to.be.true;

            result = isObject(objInst);
            expect(result).to.be.true;
        });

        it('returns false when the item is an array', () =>
        {
            const result = isObject([]);
            expect(result).to.be.false;
        });

        it('returns false for null or undefined or false', () =>
        {
            let result = isObject(null);
            expect(result).to.be.false;

            result = isObject(undefined);
            expect(result).to.be.false;

            result = isObject(false);
            expect(result).to.be.false;
        });

        it('returns false for Date objects', () =>
        {
            const result = isObject(new Date());
            expect(result).to.be.false;
        });
    });

    describe('mergeDeep()', () =>
    {
        it('merges two simple objects', () =>
        {
            const source = {
                foo: {
                    bar: 'apples'
                }
            };

            const target = {
                foo: {
                    baz: 'goats'
                },
                otherFoo: 3,
                otherOtherFoo: {
                    bar: 'omg'
                }
            };

            const results = mergeDeep(target, source);

            expect(results).to.not.be.undefined;
            expect(results).to.have.property('foo');
            expect(results).to.have.property('otherFoo', 3);
            expect(results).to.have.property('otherOtherFoo');

            expect(results.foo).to.have.keys([ 'bar', 'baz' ]);
            expect(results.foo).to.have.property('bar', 'apples');
            expect(results.foo).to.have.property('baz', 'goats');

            expect(results.otherOtherFoo).to.have.property('bar', 'omg');
        });

        it('does not merge arrays', () =>
        {
            const source = {
                foo: [ 1, 2, 3 ]
            };

            const target = {
                foo: [ 4, 5, 6 ]
            };

            const results = mergeDeep(target, source);
            expect(results.foo).to.have.members([ 1, 2, 3 ]);
        });

        it('merged deeply nested objects', () =>
        {
            const source = {
                http: {
                    port: 4545
                },
                redis: {
                    server: {
                        host: 'bar-svr'
                    },
                    options: {
                        logging: true
                    }
                }
            };

            const target = {
                http: {
                    secure: false,
                    port: 23456
                },
                redis: {
                    server: {
                        host: 'foo-svr',
                        port: 6379
                    },
                    options: {
                        secure: true
                    }
                }
            };

            const results = mergeDeep(target, source);

            expect(results).to.have.property('http');
            expect(results.http).to.have.property('secure', false);
            expect(results.http).to.have.property('port', 4545);

            expect(results).to.have.property('redis');
            expect(results.redis).to.have.property('server');
            expect(results.redis.server).to.have.property('host', 'bar-svr');
            expect(results.redis.server).to.have.property('port', 6379);
            expect(results.redis).to.have.property('options');
            expect(results.redis.options).to.have.property('secure', true);
            expect(results.redis.options).to.have.property('logging', true);
        });

        it('merges multiple simple objects', () =>
        {
            const source1 = {
                foo: {
                    baz: 'goats'
                },
                otherFoo: 3,
                otherOtherFoo: {
                    bar: 'omg'
                }
            };

            const source2 = {
                foo: {
                    bar: 'apples'
                }
            };

            const results = mergeDeep({}, source1, source2);

            expect(results).to.not.be.undefined;
            expect(results).to.have.property('foo');
            expect(results).to.have.property('otherFoo', 3);
            expect(results).to.have.property('otherOtherFoo');

            expect(results.foo).to.have.keys([ 'bar', 'baz' ]);
            expect(results.foo).to.have.property('bar', 'apples');
            expect(results.foo).to.have.property('baz', 'goats');

            expect(results.otherOtherFoo).to.have.property('bar', 'omg');
        });
    });
});

// ---------------------------------------------------------------------------------------------------------------------
