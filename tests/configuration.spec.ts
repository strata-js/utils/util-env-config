// ---------------------------------------------------------------------------------------------------------------------
// Unit Tests for the configuration.spec.ts module.
// ---------------------------------------------------------------------------------------------------------------------

import { expect } from 'chai';

// To be tested
import configMan from '../src/lib/configuration';

// ---------------------------------------------------------------------------------------------------------------------

describe('Configuration Manager', () =>
{
    before(() =>
    {
        // Reset defaults
        configMan.setConfig({});
        configMan.setEnv('local');
    });

    describe('setEnv()', () =>
    {
        it('sets the environment', () =>
        {
            configMan.setEnv('foobar');

            expect(configMan.env).to.equal('foobar');
        });
    });

    describe('setConfig()', () =>
    {
        it('sets the configuration directly', () =>
        {
            configMan.setConfig({ http: { secure: false, port: 4545 } });

            expect(configMan.config).to.have.property('http');
            expect(configMan.config.http).to.have.property('secure', false);
            expect(configMan.config.http).to.have.property('port', 4545);
        });

        it('allows the configuration to be set multiple times', () =>
        {
            configMan.setConfig({ http: { secure: false, port: 4545 } });

            expect(configMan.config).to.have.property('http');
            expect(configMan.config.http).to.have.property('secure', false);
            expect(configMan.config.http).to.have.property('port', 4545);

            configMan.setConfig({ http2: { secure: false, port: 4545 } });

            expect(configMan.config).to.not.have.property('http');
            expect(configMan.config).to.have.property('http2');
            expect(configMan.config.http2).to.have.property('secure', false);
            expect(configMan.config.http2).to.have.property('port', 4545);
        });
    });

    describe('parseConfig()', () =>
    {
        it('supports setting a basic config', () =>
        {
            configMan.parseConfig({ http: { secure: false, port: 4545 } });

            expect(configMan.config).to.have.property('http');
            expect(configMan.config.http).to.have.property('secure', false);
            expect(configMan.config.http).to.have.property('port', 4545);
        });

        describe('Environment-based configuration', () =>
        {
            it('supports overriding configuration based on environment', () =>
            {
                const config = {
                    foo: {
                        baz: 'goats'
                    },
                    otherFoo: 3,
                    otherOtherFoo: {
                        bar: 'omg'
                    },
                    environments: {
                        mocha: {
                            foo: {
                                bar: 'apples'
                            }
                        }
                    }
                };

                configMan.setEnv('mocha');
                configMan.parseConfig(config);

                expect(configMan.config).to.not.be.undefined;
                expect(configMan.config).to.have.property('foo');
                expect(configMan.config).to.have.property('otherFoo', 3);
                expect(configMan.config).to.have.property('otherOtherFoo');

                expect(configMan.config.foo).to.have.keys([ 'bar', 'baz' ]);
                expect(configMan.config.foo).to.have.property('bar', 'apples');
                expect(configMan.config.foo).to.have.property('baz', 'goats');

                expect(configMan.config.otherOtherFoo).to.have.property('bar', 'omg');
            });

            it('supports nested overrides', () =>
            {
                const config = {
                    http: {
                        secure: false,
                        port: 23456
                    },
                    redis: {
                        server: {
                            host: 'foo-svr',
                            port: 6379
                        },
                        options: {
                            secure: true
                        }
                    },
                    environments: {
                        mocha: {
                            http: {
                                port: 4545
                            },
                            redis: {
                                server: {
                                    host: 'bar-svr'
                                },
                                options: {
                                    logging: true
                                }
                            }
                        }
                    }
                };

                configMan.setEnv('mocha');
                configMan.parseConfig(config);

                expect(configMan.config).to.have.property('http');
                expect(configMan.config.http).to.have.property('secure', false);
                expect(configMan.config.http).to.have.property('port', 4545);

                expect(configMan.config).to.have.property('redis');
                expect(configMan.config.redis).to.have.property('server');
                expect(configMan.config.redis.server).to.have.property('host', 'bar-svr');
                expect(configMan.config.redis.server).to.have.property('port', 6379);
                expect(configMan.config.redis).to.have.property('options');
                expect(configMan.config.redis.options).to.have.property('secure', true);
                expect(configMan.config.redis.options).to.have.property('logging', true);
            });

            it('does not merge arrays', () =>
            {
                const config = {
                    foo: [ 4, 5, 6 ],
                    environments: {
                        mocha: {
                            foo: [ 1, 2, 3 ]
                        }
                    }
                };

                configMan.setEnv('mocha');
                configMan.parseConfig(config);
                expect(configMan.config.foo).to.have.members([ 1, 2, 3 ]);
            });

            it('returns the default on unknown environments', () =>
            {
                const config = {
                    foo: {
                        baz: 'goats'
                    },
                    otherFoo: 3,
                    otherOtherFoo: {
                        bar: 'omg'
                    },
                    environments: {
                        mocha: {
                            foo: {
                                bar: 'apples'
                            }
                        }
                    }
                };

                configMan.setEnv('dne');
                configMan.parseConfig(config);

                expect(configMan.config).to.not.be.undefined;
                expect(configMan.config).to.have.property('foo');
                expect(configMan.config).to.have.property('otherFoo', 3);
                expect(configMan.config).to.have.property('otherOtherFoo');

                expect(configMan.config.foo).to.have.keys([ 'baz' ]);
                expect(configMan.config.foo).to.have.property('baz', 'goats');

                expect(configMan.config.otherOtherFoo).to.have.property('bar', 'omg');
            });
        });

        describe('ENV overrides', () =>
        {
            it('supports overriding configuration keys with env variables', () =>
            {
                const config = {
                    foo: {
                        baz: 'goats'
                    },
                    otherFoo: 3,
                    otherOtherFoo: {
                        bar: 'omg'
                    }
                };

                process.env['CONFIG.OTHER_FOO'] = 'goats as well';
                configMan.setEnv('mocha');
                configMan.parseConfig(config);

                expect(configMan.config).to.not.be.undefined;
                expect(configMan.config).to.have.property('otherFoo', 'goats as well');
            });

            it('supports overriding nested keys', () =>
            {
                const config = {
                    foo: {
                        baz: 'goats'
                    },
                    otherFoo: 3,
                    otherOtherFoo: {
                        bar: 'omg'
                    }
                };

                process.env['CONFIG.FOO.BAZ'] = 'testing';
                configMan.setEnv('mocha');
                configMan.parseConfig(config);

                expect(configMan.config).to.not.be.undefined;
                expect(configMan.config).to.have.property('foo');
                expect(configMan.config.foo).to.have.property('baz', 'testing');
            });
        });
    });

    describe('set()', () =>
    {
        it('sets a configuration key directly', () =>
        {
            configMan.setConfig({ http: { secure: false, port: 4545 } });
            configMan.set('isFoo', true);

            expect(configMan.config).to.have.property('isFoo', true);
        });

        it('supports setting nested items', () =>
        {
            configMan.setConfig({ http: { secure: false, port: 4545 } });
            configMan.set('http.secure', true);

            expect(configMan.config).to.have.property('http');
            expect(configMan.config.http).to.have.property('secure', true);
            expect(configMan.config.http).to.have.property('port', 4545);
        });
    });

    describe('get()', () =>
    {
        it('retrieves a configuration key directly', () =>
        {
            configMan.setConfig({ http: { secure: false, port: 4545 }, isFoo: true });
            const results = configMan.get('isFoo');

            expect(results).to.be.true;
        });

        it('supports getting nested items', () =>
        {
            configMan.setConfig({ http: { secure: false, port: 4545 }, isFoo: true });
            const results = configMan.get('http.port');

            expect(results).to.equal(4545);
        });

        it('returns a default when the key is not found', () =>
        {
            configMan.setConfig({ http: { secure: false, port: 4545 }, isFoo: true });
            const results = configMan.get('dne.port', 5678);

            expect(results).to.equal(5678);
        });
    });
});

// ---------------------------------------------------------------------------------------------------------------------
